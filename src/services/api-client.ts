import axios from 'axios'
import {BASE_API_URL} from '../config/constants'

const client = axios.create({
  baseURL: BASE_API_URL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
})

export default client
