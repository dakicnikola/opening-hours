import client from "../api-client";

async function getPlaceDetails(id: string) {
  const response = await client.get(id)
  return response.data
}

export { getPlaceDetails }
