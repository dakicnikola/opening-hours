import { Space, Typography } from "antd";
import { useMemo } from 'react';

import type { TDayName, TOpeningHoursResponse, TWorkHoursResponse } from "../../PlaceDetailsCard/useGetPlaceDetails";
import { daysOfWeek } from "../constants";

const isEqual = (a: any, b: any) => JSON.stringify(a) === JSON.stringify(b)


type TWorkingHoursScheduleProps = {
  openingHours?: TOpeningHoursResponse
}

type TWorkTimeScheduleItem = {
  startDay: TDayName,
  endDay: TDayName | null,
  workTime: TWorkHoursResponse[]
}

const WorkingHoursSchedule = ({ openingHours }: TWorkingHoursScheduleProps) => {


  const workHours = useMemo(() => {
    if (!openingHours) {
      return []
    }
    const days = openingHours.days
    return daysOfWeek.reduce((acc, dayName) => {
      if (dayName === 'monday' || !isEqual(days[dayName] || [], acc[acc.length - 1].workTime)) {
        acc.push({
          startDay: dayName,
          endDay: null,
          workTime: days[dayName] || []
        })
      } else {
        acc[acc.length - 1].endDay = dayName
      }
      return acc
    }, [] as TWorkTimeScheduleItem[])

  }, [openingHours])


  return (
    <Space direction={"vertical"} style={{ width: '100%', padding: 20 }}>
      {workHours?.map(({ startDay, endDay, workTime }) =>
        (<Space key={startDay} align={"start"} style={{ justifyContent: 'space-between', width: '100%' }}>
            <Typography.Text style={{ textTransform: 'capitalize' }}>
              {endDay ? `${startDay} - ${endDay}` : startDay}
            </Typography.Text>
            <Space direction={"vertical"} align={"center"} style={{ width: 100 }}>
              {workTime.map(({ start, end }, index) => (
                <Typography.Text key={index}>{`${start} - ${end}`}</Typography.Text>
              ))}
              {!workTime.length &&
                <Typography.Text>Closed</Typography.Text>
              }
            </Space>
          </Space>
        )
      )}
    </Space>
  );
};

export default WorkingHoursSchedule;
