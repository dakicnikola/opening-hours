import type { TDayName } from "../PlaceDetailsCard/useGetPlaceDetails";

const daysOfWeek: TDayName[] = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday",
]

export { daysOfWeek}
