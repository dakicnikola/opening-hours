import { faClock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Collapse, Space, Typography } from "antd";

import type { TOpeningHoursResponse } from "../PlaceDetailsCard/useGetPlaceDetails";
import { useCheckIfWorkingNow } from "./useCheckIfWorkingNow";
import WorkingHoursSchedule from "./WorkingHoursSchedule/WorkingHoursSchedule";

type TWorkingHoursCollapseProps = {
  openingHours?: TOpeningHoursResponse
  currentTime?: string
}

const WorkingHoursCollapse = ({ openingHours, currentTime = undefined }: TWorkingHoursCollapseProps) => {

  const { isOpen, message } = useCheckIfWorkingNow(openingHours, currentTime)

  return (
    <Collapse expandIconPosition={"end"} ghost style={{ width: '100%' }}>
      <Collapse.Panel
        header={
          <Space>
            <FontAwesomeIcon icon={faClock} />
            <Typography.Text type={isOpen ? "success" : "danger"}>
              {isOpen ? 'Open now' : 'Closed'}
            </Typography.Text>
            {message &&
              <Typography.Text>
                {message}
              </Typography.Text>
            }
          </Space>
        }
        key={"opening-hours"}
      >
        <WorkingHoursSchedule openingHours={openingHours} />
      </Collapse.Panel>
    </Collapse>
  );
};

export default WorkingHoursCollapse;
