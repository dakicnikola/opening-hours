import { useMemo } from "react";

import type { TOpeningHoursResponse } from "../PlaceDetailsCard/useGetPlaceDetails";
import { daysOfWeek } from "./constants";

function useCheckIfWorkingNow(openingHours?: TOpeningHoursResponse, currentTime?: string) {

  return useMemo(() => {

      const now = new Date(currentTime || '')
      const weekDayIndex = (now.getDay() === 0 ? 7 : now.getDay()) - 1

      const today = daysOfWeek[weekDayIndex]
      const nowHours = now.getHours()
      const nowMinutes = now.getMinutes()
      const nowHoursAndMinutes = `${setZeroDigit(nowHours)}:${setZeroDigit(nowMinutes)}`

      let isOpen = false
      if (openingHours?.days[today]) {


        isOpen = openingHours?.days[today].some(({ start, end, type }) =>
          type === 'OPEN' && start <= nowHoursAndMinutes && (nowHoursAndMinutes < end || start > end)
        )
      }

      let message

      if (isOpen) {
        let targetPeriod = openingHours?.days[today].find(({ start, end }) => {
          return start <= nowHoursAndMinutes && (nowHoursAndMinutes < end || start > end)
        })
        if (targetPeriod) {
          message = `Closes at ${targetPeriod.end}`
        }
      } else {
        let minimumStartTime: string | undefined

        let dayIndex
        let i = 0;
        while (i < 8 && !minimumStartTime) {
          dayIndex = (i + weekDayIndex) % 7
          if (openingHours?.days[daysOfWeek[dayIndex]]) {
            openingHours?.days[daysOfWeek[dayIndex]]
              .forEach(({ start }) => {
                if (start > nowHoursAndMinutes || i > 0) {
                  if (!minimumStartTime) {
                    minimumStartTime = start
                  } else {
                    minimumStartTime = minimumStartTime < start ? minimumStartTime : start
                  }
                }
              })
          }
          i++
        }

        if (minimumStartTime) {
          if (dayIndex === weekDayIndex && minimumStartTime > nowHoursAndMinutes) {
            message = `Opens at ${minimumStartTime}`
          } else if (dayIndex !== undefined) {
            message = `Opens at ${daysOfWeek[dayIndex]} ${minimumStartTime}`
          }
        } else {
          message = 'Permanently'
        }
      }


      return { isOpen, message }
    }, [openingHours, currentTime]
  )

}

const setZeroDigit = (digit: number) => digit < 10 ? `0${digit}` : digit

export { useCheckIfWorkingNow }
