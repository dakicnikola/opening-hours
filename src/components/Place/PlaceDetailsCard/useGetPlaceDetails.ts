import { useEffect, useState } from "react";

import { getPlaceDetails } from "../../../services/api/place";


function useGetPlaceDetails(placeId: string) {
  const [placeDetails, setPlaceDetails] = useState<TPlaceDetailsResponse | null>(null)

  useEffect(() => {
    if (placeId) {
      getPlaceDetails(placeId)
        .then(setPlaceDetails)
    }
  }, [placeId])

  return placeDetails
}

type TPlaceDetailsResponse = {
  displayed_what: string
  displayed_where: string
  opening_hours: TOpeningHoursResponse
  place_feedback_summary: {
    average_rating: number,
    display_average_rating: boolean
  }
}

type TOpeningHoursResponse = {
  days: Record<TDayName, TWorkHoursResponse[]>,
  closed_on_holidays: boolean,
  open_by_arrangement: boolean
}

type TDayName = 'monday' | 'tuesday' | 'wednesday' | 'thursday' | 'friday' | 'saturday' | 'sunday'

type TWorkHoursResponse = {
  "start": string
  "end": string
  "type": "OPEN" | "CLOSED"
}

export type { TOpeningHoursResponse, TDayName, TWorkHoursResponse }

export { useGetPlaceDetails }
