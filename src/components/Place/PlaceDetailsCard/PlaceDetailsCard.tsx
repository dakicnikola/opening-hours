import { faLocationDot } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card, List, Space, Typography } from "antd";
import ReactStars from 'react-stars'

import WorkingHoursCollapse from "../WorkingHoursCollapse/WorkingHoursCollapse";
import { useGetPlaceDetails } from "./useGetPlaceDetails";


type TPlaceDetailsCardProps = {
  placeId: string
  currentTime?: string
}


const PlaceDetailsCard = ({ placeId, currentTime }: TPlaceDetailsCardProps) => {

  const placeDetails = useGetPlaceDetails(placeId)

  return (
    <Card
      title={
        <Space direction={"vertical"} size={"small"} align={"center"}>
          <Typography.Title>{placeDetails?.displayed_what}</Typography.Title>
          {placeDetails?.place_feedback_summary?.display_average_rating &&
            <ReactStars
              count={5}
              size={18}
              edit={false}
              value={placeDetails?.place_feedback_summary?.average_rating}
              color2={'#ffd700'}
            />
          }
        </Space>
      }
      headStyle={{ border: 0 }}
      style={{
        width: 500,
        border: '1px solid black'
      }}
      bordered={true}
    >
      <List bordered={false}>
        {Boolean(placeDetails?.displayed_where) &&
          <List.Item>
             <Space>
                <FontAwesomeIcon icon={faLocationDot} />
                <Typography.Text>
                  {placeDetails?.displayed_where}
                </Typography.Text>
             </Space>
          </List.Item>
        }
        {
          Boolean(placeDetails?.["opening_hours"]) &&
          <List.Item>
             <WorkingHoursCollapse openingHours={placeDetails?.["opening_hours"]} currentTime={currentTime} />
          </List.Item>
        }
      </List>
    </Card>
  );
};

export default PlaceDetailsCard;
