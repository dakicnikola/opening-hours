import { DatePicker, Space, TimePicker, Typography } from "antd";
import type { Dayjs } from 'dayjs';
import dayjs from 'dayjs';
import { useState } from "react";

import './App.css';
import PlaceDetailsCard from "./components/Place/PlaceDetailsCard/PlaceDetailsCard";
import { daysOfWeek } from "./components/Place/WorkingHoursCollapse/constants";


const placeIdExample1 = 'GXvPAor1ifNfpF0U5PTG0w'
const placeIdExample2 = 'ohGSnJtMIC5nPfYRi_HTAg'

function App() {

  const [currentTime, setCurrentTime] = useState<Dayjs>(() => {
    return dayjs()
  })

  const onDateChange = (date: Dayjs | null) => {
    setCurrentTime((prevState) => {
      const newDate = dayjs(date)
      return newDate.hour(prevState.hour()).minute(prevState.minute())
    })
  }
  const onTimeChange = (date: Dayjs | null) => {
    setCurrentTime((prevState) => {
      const newDate = dayjs(date)
      return newDate.date(prevState.date()).year(prevState.year())
    })
  }

  return (
    <Space direction={"vertical"} align={"center"} style={{ margin: 20 }}>
      <Space align={"start"}>
        <Space direction={"vertical"} align={"center"}>
          <DatePicker onChange={onDateChange} value={currentTime} />
          <Typography.Text style={{ textTransform: 'capitalize' }}>
            {daysOfWeek[currentTime.day() === 0 ? 6 : currentTime.day() - 1]}
          </Typography.Text>
        </Space>
        <TimePicker format={"HH:mm"} onChange={onTimeChange} value={currentTime} />
      </Space>

      <Space style={{ margin: 20 }} size={"large"}>
        <PlaceDetailsCard placeId={placeIdExample1} currentTime={currentTime.toString()} />
        <PlaceDetailsCard placeId={placeIdExample2} currentTime={currentTime.toString()} />
      </Space>
    </Space>
  );
}

export default App;
